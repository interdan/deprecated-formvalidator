export default function getFixedTopHeight(topPaddingForFixedElementClass): number  {
  if (!topPaddingForFixedElementClass) return 0;

  const element = document.querySelector(`.${topPaddingForFixedElementClass}`);
  if (!element) return 0;

  const computedStyle = window.getComputedStyle(element, null);
  const paddingTop = computedStyle.getPropertyValue('padding-top');
  return parseInt(paddingTop, 10) || 0;
}
