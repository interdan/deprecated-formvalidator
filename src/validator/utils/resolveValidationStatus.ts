import IValidationStatus from '../types/IValidationStatus';

export default function resolveValidationStatus(
  propertyId: string,
  isValid: boolean,
  showError: boolean | null,
  validationStatus: IValidationStatus,
) {
  const newValue = {
    isValid,
    showError: isValid ? false : showError,
  };

  if (typeof newValue.showError !== 'boolean') {
    const currentValue = validationStatus[propertyId];
    newValue.showError = currentValue ? currentValue.showError : false;
  }

  const oldValue = validationStatus[propertyId];
  const valueChanged = !oldValue ||
    oldValue.isValid !== newValue.isValid ||
    oldValue.showError !== newValue.showError;

  if (valueChanged) {
    return { validationStatus: { ...validationStatus, [propertyId]: newValue } };
  }

  return null;
}
