import ReactDOM from 'react-dom';
import IPosition from '../types/IPosition';

export default function getPosition(): IPosition {
  let node: HTMLInputElement = ReactDOM.findDOMNode(this) as HTMLInputElement;
  let x: number = node.offsetLeft;
  let y: number = node.offsetTop;

  while (node.offsetParent) {
    node = node.offsetParent as HTMLInputElement;
    x += node.offsetLeft;
    y += node.offsetTop;
  }

  return { x, y };
}
