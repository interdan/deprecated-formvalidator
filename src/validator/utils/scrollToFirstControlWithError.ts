import animateScrollTo from 'animated-scroll-to';
import getFixedTopHeight from './getFixedTopHeight';
import findSmallestItemInArray from './findSmallestItemInArray';
import IPositionGetters from '../types/IPositionGetters';
import IValidationStatus from '../types/IValidationStatus';
import IFormPropertyStatusValue from '../types/IFormPropertyStatusValue';

const SCROLL_TO_ERROR_SPEED_MS = 500;

const filterOnlyInvalidItems = ([, { isValid }]: [string, IFormPropertyStatusValue]): boolean => !isValid;

const getFirstWrongDataYPosition = (validationStatus: IValidationStatus, positionGetters: IPositionGetters): number => {
  const topPositionArray = Object.entries(validationStatus)
    .filter(filterOnlyInvalidItems)
    .map(([key]: any): number => positionGetters[key]().y);

  return findSmallestItemInArray(topPositionArray);
};

export default function scrollToFirstControlWithError(
  validationStatus: IValidationStatus,
  positionGetters: IPositionGetters,
  topPaddingForFixedElementClass: string,
) {

  const scrollToValue = getFirstWrongDataYPosition(validationStatus, positionGetters) - getFixedTopHeight(topPaddingForFixedElementClass);

  animateScrollTo(scrollToValue, { speed: SCROLL_TO_ERROR_SPEED_MS });
}
