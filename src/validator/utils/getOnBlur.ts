import React, { ReactElement } from 'react';

export default function getOnBlur({ props }: ReactElement<any>) {
  let returnValue = null;

  if (props.onBlur) {
    returnValue = props.onBlur;
  } else if (props.children) {
    returnValue = React.Children.map(props.children, child => child.props.onBlur)[0];
  }

  return typeof returnValue === 'function' ? returnValue : null;
}
