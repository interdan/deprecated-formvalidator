export default function findSmallestItemInArray(arr: number[]): number {
  return arr.reduce(
    (result, item) => (item < result ? item : result),
    arr[0],
  );
}
