import getValueFromChildren from './getValueFromChildren';
import { ReactNode } from 'react';
import { checkerFunctionType } from '..';

export default function checkIsValueValid(
  isTrivialValidation: boolean, valueIsEmpty: boolean, checker: checkerFunctionType, children: ReactNode,
): boolean {

  if (isTrivialValidation) return !valueIsEmpty;

  return typeof checker !== 'function'
    || !!checker(getValueFromChildren(children));
}
