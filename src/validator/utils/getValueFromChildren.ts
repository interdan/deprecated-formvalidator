import React, { ReactNode } from 'react';

export default function getValueFromChildren(children: ReactNode) {
  // we expect only 1 element in children collection
  const childrenValues = React.Children.map(
    children,
    (child: React.ReactElement<any>) => {

      const { value } = child.props;
      if (typeof value !== 'undefined') {
        return child.props.value;
      }

      if (child.props.children) {
        return React.Children.map(child.props.children, _child => _child.props.value)[0];
      }
    });

  return childrenValues[0] || '';
}
