import React from 'react';
import resolveValidationStatus from './utils/resolveValidationStatus';
import scrollToFirstControlWithError from './utils/scrollToFirstControlWithError';
import setValidationStatusType from './types/setValidationStatusType';
import setPositionGetterType from './types/setPositionGetterType';
import detachPropertyType from './types/detachPropertyType';
import IFormPropertyStatusValue from './types/IFormPropertyStatusValue';
import getPositionType from './types/getPositionType';
import IPositionGetters from './types/IPositionGetters';
import IValidationStatus from './types/IValidationStatus';

interface IContext {
  validationStatus: IValidationStatus;
  setValidationStatus: setValidationStatusType;
  detachProperty: detachPropertyType;
  setPositionGetter: setPositionGetterType;
}

const { Provider, Consumer } = React.createContext<IContext>({
  validationStatus: {},
  setValidationStatus: () => null,
  setPositionGetter: () => null,
  detachProperty: () => null,
});

export { Consumer };

interface IWithFormValidationSettings {
  topPaddingForFixedElementClass?: string;
}

const getDefaultSettings = () => ({
  topPaddingForFixedElementClass: '',
});

interface IEnhancedState {
  validationStatus: IValidationStatus;
}

interface IProps {
  [key: string]: any;
}

export interface IWithFormValidationComponentProps extends IProps {
  isFormValid: boolean;
  makeErrorsVisible: () => void;
}

type withFormValidationType = <P extends object>(
  Enhanceable: React.ComponentType<IWithFormValidationComponentProps>,
  customSettings?: IWithFormValidationSettings,
) => React.ComponentType<P>;

const withFormValidation: withFormValidationType = <P extends object>(
  Enhanceable,
  customSettings = {}) => (
    class Enhanced extends React.Component<P, IEnhancedState> {
      public positionGetters: IPositionGetters = {};

      public state: IEnhancedState = {
        validationStatus: {},
      };

      public settings: IWithFormValidationSettings = { ...getDefaultSettings(), ...customSettings };

      public setPositionGetter: setPositionGetterType = (propertyId: string, getPosition: getPositionType) => {
        this.positionGetters[propertyId] = getPosition;
      }

      public setValidationStatus: setValidationStatusType = (
        propertyId: string,
        isValid: boolean,
        showError: boolean | null = null,
      ) => this
        .setState(({ validationStatus }) => resolveValidationStatus(propertyId, isValid, showError, validationStatus))

      public isFormValid = () => Object.values(this.state.validationStatus)
        .every(({ isValid }: IFormPropertyStatusValue) => isValid)

      public makeErrorsVisible = () => {
        this.showAllErrorMessages();

        if (!this.isFormValid()) {
          scrollToFirstControlWithError(
            this.state.validationStatus,
            this.positionGetters,
            this.settings.topPaddingForFixedElementClass,
          );
        }
      }

      public detachProperty: detachPropertyType = (propertyNameToRemove: string) => {
        this.setState(state => {
          const { [propertyNameToRemove]: removedValue, ...validationStatus } = state.validationStatus;
          return { validationStatus };
        });

        const { [propertyNameToRemove]: removedGetter, ...restGetters } = this.positionGetters;
        this.positionGetters = restGetters;
      }

      public showAllErrorMessages = () => {
        const validationStatus = {};

        Object.entries(this.state.validationStatus).forEach(([key, { isValid }]) => {
          validationStatus[key] = { isValid, showError: !isValid };
        });

        this.setState(() => ({ validationStatus }));
      }

      public render() {
        const validationState = {
          validationStatus: this.state.validationStatus,
          setValidationStatus: this.setValidationStatus,
          setPositionGetter: this.setPositionGetter,
          detachProperty: this.detachProperty,
        };

        return (
          <Provider value={validationState}>
            <Enhanceable {...this.props} isFormValid={this.isFormValid()} makeErrorsVisible={this.makeErrorsVisible} />
          </Provider>
        );
      }
    });

export default withFormValidation;
