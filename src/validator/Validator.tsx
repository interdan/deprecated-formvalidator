import * as React from 'react';
import nanoid from 'nanoid/non-secure';
import classNames from 'classnames';

import { Consumer } from './withFormValidation';

import './styles.scss';
import IFormPropertyStatusValue from './types/IFormPropertyStatusValue';
import setPositionGetterType from './types/setPositionGetterType';
import detachPropertyType from './types/detachPropertyType';
import setValidationStatusType from './types/setValidationStatusType';
import getOnBlur from './utils/getOnBlur';
import getValueFromChildren from './utils/getValueFromChildren';
import getPosition from './utils/getPosition';
import checkerFunctionType from './types/checkerFunctionType';
import checkIsValueValid from './utils/checkIsValueValid';

interface IValidatorProps {
  errorText: React.ReactNode;
  children: React.ReactNode;
  className?: string;
  errorClass?: string;
  checker?: checkerFunctionType;
  valueIsEmpty?: boolean;
}

export default class Validator extends React.Component<IValidatorProps> {
  private validatorId: string;
  private valueIsEmptyChanged: boolean = false;

  constructor(props: any) {
    super(props);
    this.validatorId = nanoid();
  }

  public componentDidMount() {
    this.setPositionGetter(this.validatorId, getPosition.bind(this));

    const showErrorIfInvalid = !this.isTrivialValidation && !!getValueFromChildren(this.props.children);
    this.updateIsValidState(showErrorIfInvalid);
  }

  public componentWillUpdate(nextProps) {
    this.valueIsEmptyChanged = this.isTrivialValidation && this.props.valueIsEmpty !== nextProps.valueIsEmpty;
  }

  public componentDidUpdate() {
    this.updateIsValidState(this.valueIsEmptyChanged);
  }

  public componentWillUnmount() {
    this.detachProperty(this.validatorId);
  }

  private setPositionGetter: setPositionGetterType;
  private detachProperty: detachPropertyType;
  private setValidationStatus: setValidationStatusType;
  private originalOnBlur: null | (() => void);
  private isTrivialValidation = Object.prototype.hasOwnProperty.call(this.props, 'valueIsEmpty');

  private handleInputOnBlur = () => {
    this.updateIsValidState(true);

    if (this.originalOnBlur) {
      this.originalOnBlur();
    }
  }

  private updateIsValidState = (updateShowError: boolean = true) => {
    const { checker, valueIsEmpty, children } = this.props;
    const isValid = checkIsValueValid(this.isTrivialValidation, valueIsEmpty, checker, children);

    const showError = (updateShowError === false) ? null : !isValid;
    this.setValidationStatus(this.validatorId, isValid, showError);
  }

  public render() {
    const { className, checker, errorText, errorClass,
      children, ...childrenProps } = this.props;

    const resultChildren = this.isTrivialValidation ?
      children :
      React.Children.map(children, (child: React.ReactElement<any>) => {
        this.originalOnBlur = getOnBlur(child);

        (childrenProps as any).onBlur = this.handleInputOnBlur;
        return React.cloneElement(child, childrenProps);
      });

    return (
      <Consumer>
        {({ validationStatus, setValidationStatus, detachProperty, setPositionGetter }) => {
          this.setPositionGetter = setPositionGetter;
          this.setValidationStatus = setValidationStatus;
          this.detachProperty = detachProperty;
          const propertyStatus: IFormPropertyStatusValue = validationStatus[this.validatorId];
          const showError = propertyStatus && propertyStatus.showError;

          const classNameValue = classNames({
            [className]: !!className,
            [errorClass || 'error']: showError,
          });

          return (
            <div className={classNameValue || undefined}>
              <div className="validated-control">{resultChildren}</div>
              {showError && <div className="error-message">{errorText}</div>}
            </div>
          );
        }
        }
      </Consumer>
    );
  }
}
