import getPositionType from './getPositionType';

type setPositionGetterType = (propertyId: string, getPosition: getPositionType) => void;

export default setPositionGetterType;
