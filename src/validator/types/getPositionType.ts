import IPosition from './IPosition';

type getPositionType = () => IPosition;

export default getPositionType;
