import getPositionType from './getPositionType';

interface IPositionGetters {
  [key: string]: getPositionType;
}

export default IPositionGetters;
