import IFormPropertyStatusValue from './IFormPropertyStatusValue';

interface IValidationStatus {
  [key: string]: IFormPropertyStatusValue;
}

export default IValidationStatus;
