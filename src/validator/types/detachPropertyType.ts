type detachPropertyType = (propertyNameToRemove: string) => void;

export default detachPropertyType;
