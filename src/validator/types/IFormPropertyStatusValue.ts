interface IFormPropertyStatusValue {
  showError: boolean;
  isValid: boolean;
}

export default IFormPropertyStatusValue;
