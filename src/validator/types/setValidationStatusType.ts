type setValidationStatusType = (propertyId: string, isValid: boolean, showError: boolean | null) => void;
export default setValidationStatusType;
