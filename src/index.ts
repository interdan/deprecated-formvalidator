export { checkerFunctionType, withFormValidation, IWithFormValidationComponentProps } from './validator';
import Validator from './validator';

export default Validator;
