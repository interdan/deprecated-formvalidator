# React validator
Library allows to achieve three main goals for your React application:

* check the input value for correctness, show/hide an error message

* check whether or not all the inputs contain valid values

* show all the errors to the user, scroll automatically to the first input that contains the error

## Installation
    npm install @interdan/react-validator

## Example
```jsx
import React, { useState } from 'react';
import Validator, { withFormValidation } from '@interdan/react-validator';

const SimpleTextInput = ({ label, checker, errorMessage }) => {
  const [value, setTextValue] = useState('');

  const onChange = (e: any) => setTextValue(e.target.value);

  return (
    <div className="form-control">
      <div className="label">{label}</div>
      <div className="input">
        <Validator errorText={errorMessage} checker={checker}>
          <input type="text" value={value} onChange={onChange} />
        </Validator>
      </div>
    </div>
  );
};

...

const NOT_EMPTY_VALUE_ERROR_TEXT = "Value can't be empty";
const NUMBER_REQUIRED_ERROR_TEXT = 'Please, provide a positive integer';

const notEmptyChecker = (value) => !!value;

const isPositiveNumber = (value) => {
  const intValue = parseInt(value, 10);
  return !!intValue && intValue > 0;
};

const Form = ({ isFormValid, makeErrorsVisible, proceed }) => {
  const handleOnProceed = () => {
    if (!isFormValid) {
      makeErrorsVisible();
      return;
    }

    proceed();
  };

  return (
    <div className="form">
      <SimpleTextInput checker={notEmptyChecker} label="First name" errorMessage={NOT_EMPTY_VALUE_ERROR_TEXT} />
      <SimpleTextInput checker={notEmptyChecker} label="Last name" errorMessage={NOT_EMPTY_VALUE_ERROR_TEXT} />
      <SimpleTextInput checker={isPositiveNumber} label="Your age" errorMessage={NUMBER_REQUIRED_ERROR_TEXT} />
      <button onClick={handleOnProceed}>Proceed</button>
    </div>
  );
};

export default withFormValidation(Form);
```

`withFormValidation` is a higher-order component (HOC), that provide your component with two props:

* `isFormValid` - boolean variable that indicates whether or not all the inputs contain valid values;
* `makeErrorsVisible` - the parameterless method that shows all the errors, if it is necessary - scrolls (with animation) up to the topmost input with wrong data. Of course, you can just disable your "Proceed" button but it's not always obvious what is wrong with entered data for your end-user. The best example when a user gets confused is a situation when he didn't even try to enter any data (just never focused on an input, so the error is hidden). To achieve a better UX it's a good idea to leave your button available and just show what is wrong when the user clicks on it.

`Validator` - is the React component that should wrap the component the value of which you want to check. You must always provide `errorText` property with some string or React element value type. There is several ways of how you can use `Validator` within your app:

* just with `valueIsEmpty` property: if this boolean prop is set - `true` means value is correct and `false` - value is incorrect. You can use this approach when you need to be sure that the select's value isn't empty, or chech checkbox input or some specific custom control. It's a trivial check of course, but the validator will show/hide error for your app, and will use this data for computing the "isFormValid" value;

* with `checker` function. As you can see from the example this function gets only one parameter - input's value and must check whether it correct or not. The result of this checker is cast to boolean type under the hood. So both of the checker functions below are considered as valid:
```javascript
// value of string type
const notEmptyChecker = value => !!value;

// or just
const notEmptyChecker = value => value;
```

With `checker` function you can test any React component that stores the value in a `value` prop. This `value` is passed to checker function.

`Validator` also has two more optional props: `className` and `errorClass` that you can use to set some custom css classes for the whole Validator container and error message container accordingly.