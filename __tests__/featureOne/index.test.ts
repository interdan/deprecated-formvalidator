import featureOne from '../../src/featureOne';

describe('feature: one', () => {
  it('should return "Hello world" if no param passed', () => {
    expect(featureOne()).toBe('Hello world!');
  });

  it('should return "Hello user" if "user" is passed as param', () => {
    expect(featureOne('user')).toBe('Hello user!');
  });
});
