import React, { useState } from 'react';
import './styles.scss';
import Form from './Form';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <h2>Validated form example</h2>
        <Form />
      </div>
    );
  }
}

export default App;
