import React from 'react';
import { withFormValidation, IWithFormValidationComponentProps } from '@interdan/react-validator';
import SimpleTextInput from './SimpleTextInput';
import testData from './formDataTestMock';

const Form = ({ isFormValid, makeErrorsVisible }: IWithFormValidationComponentProps) => {
  const handleOnProceed = () => {
    if (!isFormValid && makeErrorsVisible) {
      makeErrorsVisible();
    }
  };

  return (
    <div className="form">
      {testData.map((props, index) =>
        <SimpleTextInput key={index} {...props} />)}

      <button disabled={!isFormValid}>Proceed</button>
      <button onClick={handleOnProceed}>Proceed (not blocking)</button>
    </div>
  );
};

export default withFormValidation(Form);
