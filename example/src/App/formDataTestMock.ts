import { ISimpleTextInputProps } from './SimpleTextInput';

const notEmptyChecker = (value: string) => !!value;
const isNumber = (value: string) => !isNaN(parseInt(value, 10));

const testInputs: ISimpleTextInputProps[] = [
  {
    label: 'First name',
    checker: notEmptyChecker,
    errorMessage: "Value can't be empty",
  },
  {
    label: 'Last name',
    checker: notEmptyChecker,
    errorMessage: "Value can't be empty",
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
  {
    label: 'Age',
    checker: isNumber,
    errorMessage: 'Value must be a number',
  },
];

export default testInputs;
