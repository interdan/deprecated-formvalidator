import React, { useState } from 'react';
import Validator, { checkerFunctionType } from '@interdan/react-validator';

export interface ISimpleTextInputProps {
  label: string;
  checker: checkerFunctionType;
  errorMessage: string;
}

const SimpleTextInput = ({ label, checker, errorMessage }: ISimpleTextInputProps) => {
  const [value, setTextValue] = useState('');

  const onChange = (e: any) => setTextValue(e.target.value);

  return (
    <div className="form-control">
      <div className="label">{label}</div>
      <div className="input">
        <Validator errorText={errorMessage} checker={checker}>
          <input type="text" value={value} onChange={onChange} />
        </Validator>
      </div>
    </div>
  );
};

export default SimpleTextInput;
